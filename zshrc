# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="spaceship"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
 export UPDATE_ZSH_DAYS=1

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
#DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
if [[ `uname` == 'Linux' ]]
then
  export LINUX=1
  export GNU_USERLAND=1
  plugins=(git sudo npm)
  plugins+=(zsh-completions)
  autoload -U compinit && compinit
else
  export LINUX=
fi

if [[ `uname` == 'Darwin' ]]
then
  export OSX=1
  plugins=(git sudo npm osx docker zsh-syntax-highlighting zsh-nvm)
else
  export OSX=
fi
# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
# export MANPATH="/usr/local/man:$MANPATH"

source ~/.zshrc-custom
source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

export EDITOR='vim'
# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

############ Emad Customizations ##############
alias mux='tmuxinator'
alias tk='tmux kill-session -t '
alias mux="TERM=screen-256color-bce mux"
alias reload='source ~/.zshrc'
alias tmux="TERM=screen-256color-bce tmux"
alias zshconfig='vim ~/.zshrc'
alias ohmyzsh='vim ~/.oh-my-zsh'
alias cat='bat --paging=never --style=plain'
alias du="ncdu --color dark -rr -x --exclude .git --exclude node_modules"
alias lg='lazygit'
alias av='python ~/src/kbs/ansible-vault-shortcuts/av.py'

alias gffs="git flow feature start"
alias gfff="git flow feature finish"
alias gd='git diffall'
alias dm='docker-machine'
alias dc='docker-compose'

export ACKRC=".ackrc"
#export GIT_SSH=~/.ssh_settings
#export GIT_SSH="ssh -l eibrahim"

KEYTIMEOUT=1

. ~/src/os/z/z.sh

#file search alias https://msol.io/blog/tech/work-more-efficiently-on-your-mac-for-developers
#function f() { find . -iname "*$1*" ${@:2} }
#function r() { grep "$1" ${@:2} -R . }
# Even better: use the silver searcher (brew install the_silver_searcher)
alias f="ag -g"
alias r=ag

##### Docker setup for default machine
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi
if which pyenv-virtualenv-init > /dev/null; then eval "$(pyenv virtualenv-init -)"; fi
alias ddef='eval "$(docker-machine env default)"'
alias ddev='eval "$(docker-machine env dev)"'

##### change tab name ######
# $1 = type; 0 - both, 1 - tab, 2 - title
# # rest = text
setTerminalText () {
  # echo works in bash & zsh
  DISABLE_AUTO_TITLE="true"
  local mode=$1 ; shift
  echo -ne "\033]$mode;$@\007"
}
stt_both  () { setTerminalText 0 $@; }
stt_tab   () { setTerminalText 1 $@; }
stt_title () { setTerminalText 2 $@; }

export Ember_Help_Wanted_CouchDb_Url="https://eibrahim.cloudant.com/ember-help-wanted"

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
# Add Visual Studio Code (code)
export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
export PATH=/Library/Frameworks/Mono.framework/Versions/Current/bin/:${PATH}

source ~/src/dotfiles/tmuxinator.zsh

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
#eval "$(ssh-agent -s)"

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /Users/emad/.nvm/versions/node/v8.9.3/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/emad/.nvm/versions/node/v8.9.3/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /Users/emad/.nvm/versions/node/v8.9.3/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/emad/.nvm/versions/node/v8.9.3/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh

unset GIT_SSH

# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[[ -f /Users/emad/.nvm/versions/node/v7.10.1/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh ]] && . /Users/emad/.nvm/versions/node/v7.10.1/lib/node_modules/serverless/node_modules/tabtab/.completions/slss.zsh


# auto switch node version with nvm
autoload -U add-zsh-hook
load-nvmrc() {
  if [[ -f .nvmrc && -r .nvmrc ]]; then
    nvm use
  #elif [[ $(nvm version) != $(nvm version default)  ]]; then
    #echo "Reverting to nvm default version"
    #nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`
[[ -f /Users/emad/src/hyperlogs/frontend/node_modules/tabtab/.completions/electron-forge.zsh ]] && . /Users/emad/src/hyperlogs/frontend/node_modules/tabtab/.completions/electron-forge.zshexport PATH="/usr/local/opt/mysql-client/bin:$PATH"
export PATH="/usr/local/opt/curl/bin:$PATH"
