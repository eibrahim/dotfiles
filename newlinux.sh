#copy ssh keys
scp emad@mb2015:~/.ssh/*  ~/.ssh
scp emad@mb2015:~/.vim/*  ~/.vim

#NOTE: this file is located in this git repo
git clone git@bitbucket.org:eibrahim/dotfiles.git ~/src/dotfiles

# install z
git clone git@github.com:rupa/z.git ~/src/os/z

sudo apt-get update
sudo apt-get install curl

#install zsh
sudo apt-get install zsh
chsh -s /bin/zsh
mkdir ~/.oh-my-zsh/custom/plugins
git clone git://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions

sudo apt-get install tmux

sudo apt-get install ack-grep
sudo apt-get install silversearcher-ag

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm


#rbenv
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

git clone git://github.com/sstephenson/rbenv.git .rbenv
git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

#install tool with cask
#install git diff-so-fancy (needs npm)
npm install -g diff-so-fancy
#git config --global core.pager "diff-so-fancy | less --tabs=1,5 -R"

gem install tmuxinator

# clone dotfiles
ln -fs ~/src/dotfiles/zshrc ~/.zshrc
ln -fs ~/src/dotfiles/ssh_config ~/.ssh/config
ln -fs ~/src/dotfiles/ssh_settings ~/.ssh_settings
ln -fs ~/src/dotfiles/vimrc ~/.vimrc
ln -fs ~/src/dotfiles/tmux.conf ~/.tmux.conf
ln -fs ~/src/dotfiles/.tmuxinator ~
ln -fs ~/src/dotfiles/ackrc ~/.ackrc
ln -fs ~/src/dotfiles/agignore ~/.agignore
ln -fs ~/src/dotfiles/gitconfig ~/.gitconfig
ln -fs ~/src/dotfiles/gitignore_global ~/.gitignore_global
ln -fs ~/src/dotfiles/vim/colors ~/.vim
ln -fs ~/src/dotfiles/snippets ~/.vim

# vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim -c 'PluginInstall' -c 'qa!'

