#manuall install
# karabinar, hyper key,(not needed in VM)
#beyond compare

#set host name
scutil --set ComputerName "vm one"
scutil --set LocalHostName "vm1"
scutil –-set HostName "vm1"

# xcode tools
xcode-select --install

#copy ssh keys
scp emad@mb2015:~/.ssh/*  ~/.ssh
scp emad@mb2015:~/.vim/*  ~/.vim

#enable ssh
systemsetup -setremotelogin on

#Set a blazingly fast keyboard repeat rate
#defaults write NSGlobalDomain KeyRepeat -int 0.02

#Set a shorter Delay until key repeat
#defaults write NSGlobalDomain InitialKeyRepeat -int 12

#Show the ~/Library folder
chflags nohidden ~/Library


#NOTE: this file is located in this git repo
git clone git@bitbucket.org:eibrahim/dotfiles.git ~/src/dotfiles

# install z
git clone git@github.com:rupa/z.git ~/src/os/z

#install brew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew install ack tmux wget curl git git-flow zsh zsh-completions the_silver_searcher bat ncdu
brew install reattach-to-user-namespace

/usr/local/opt/fzf/install

#install zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
chsh -s /bin/zsh
mkdir ~/.oh-my-zsh/custom/plugins
git clone git://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

#install tool with cask
#brew cask install iterm2
#brew cask install google-chrome
#brew cask install gitup
brew tap caskroom/fonts
brew cask install font-hack-nerd-font

#lazygit
brew tap jesseduffield/lazygit
brew install lazygit

#install git diff-so-fancy (needs npm)
#npm install -g diff-so-fancy
#git config --global core.pager "diff-so-fancy | less --tabs=1,5 -R"

#gem install tmuxinator
gem install tmuxinator

# clone dotfiles
ln -fs ~/src/dotfiles/zshrc ~/.zshrc
ln -fs ~/src/dotfiles/ctags ~/.ctags
ln -fs ~/src/dotfiles/ssh_config ~/.ssh/config
ln -fs ~/src/dotfiles/ssh_settings ~/.ssh_settings
ln -fs ~/src/dotfiles/slate ~/.slate
ln -fs ~/src/dotfiles/vimrc ~/.vimrc
ln -fs ~/src/dotfiles/gemrc ~/.gemrc
ln -fs ~/src/dotfiles/tmux.conf ~/.tmux.conf
ln -fs ~/src/dotfiles/.tmuxinator ~
ln -fs ~/src/dotfiles/ackrc ~/.ackrc
ln -fs ~/src/dotfiles/agignore ~/.agignore
ln -fs ~/src/dotfiles/gitconfig ~/.gitconfig
ln -fs ~/src/dotfiles/gitignore_global ~/.gitignore_global
ln -fs ~/src/dotfiles/vim/colors ~/.vim
ln -fs ~/src/dotfiles/vim/bundles/ember_tools ~/.vim/bundles
ln -fs ~/src/dotfiles/snippets ~/.vim

# vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim -c 'PluginInstall' -c 'qa!'

#misc todos
#setup hyperkey (not needed for vm)
