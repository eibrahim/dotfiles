# install z
git clone git@github.com:rupa/z.git ~/src/os/z

#install zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
chsh -s /bin/zsh
mkdir ~/.oh-my-zsh/custom/plugins
git clone git://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

#gem install tmuxinator
gem install tmuxinator

# clone dotfiles
ln -fs ~/src/dotfiles/zshrc ~/.zshrc
ln -fs ~/src/dotfiles/ssh_config ~/.ssh/config
ln -fs ~/src/dotfiles/ssh_settings ~/.ssh_settings
ln -fs ~/src/dotfiles/vimrc ~/.vimrc
ln -fs ~/src/dotfiles/tmux.conf ~/.tmux.conf
ln -fs ~/src/dotfiles/.tmuxinator ~
ln -fs ~/src/dotfiles/ackrc ~/.ackrc
ln -fs ~/src/dotfiles/agignore ~/.agignore
ln -fs ~/src/dotfiles/gitconfig ~/.gitconfig
ln -fs ~/src/dotfiles/gitignore_global ~/.gitignore_global
ln -fs ~/src/dotfiles/vim/colors ~/.vim
ln -fs ~/src/dotfiles/snippets ~/.vim

# vim
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim -c 'PluginInstall' -c 'qa!'

