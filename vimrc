"" set wrap options
"set wrap linebreak nolist
set nowrap
"" set folding method
"set foldmethod=indent
"set foldlevelstart=1
"autocmd Syntax xml,html,xhtml,javascript setlocal foldmethod=syntax
"autocmd Syntax xml,html,xhtml,javascript normal zR
" Send more characters for redraws
set ttyfast

" Enable mouse use in all modes
set mouse=a

" Set this to the name of your terminal that supports mouse codes.
" Must be one of: xterm, xterm2, netterm, dec, jsbterm, pterm
set ttymouse=xterm2

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'elixir-lang/vim-elixir'
Plugin 'xolox/vim-session'
Plugin 'bling/vim-airline'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'terryma/vim-expand-region'
"Plugin 'bling/vim-bufferline'
"Bundle 'jlanzarotta/bufexplorer'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Raimondi/delimitMate'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'OmniSharp/omnisharp-vim'
"Bundle 'jistr/vim-nerdtree-tabs'
Plugin 'flazz/vim-colorschemes'
Plugin 'airblade/vim-gitgutter'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'mustache/vim-mustache-handlebars'
"Plugin 'thoughtbot/vim-rspec'
Plugin 'mileszs/ack.vim'
"Plugin 'bkad/CamelCaseMotion'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-notes'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'regedarek/ZoomWin'
"Plugin 'tpope/vim-unimpaired'
Plugin 'Valloric/MatchTagAlways'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'elentok/plaintasks.vim'
Plugin 'https://github.com/scrooloose/nerdcommenter.git'
"Plugin 'git://github.com/altercation/vim-colors-solarized.git'
"Plugin 'dsawardekar/ember.vim'
"Bundle 'dsawardekar/portkey'
Plugin 'tpope/vim-projectionist'
Plugin 'terryma/vim-multiple-cursors'
Bundle "MarcWeber/vim-addon-mw-utils"
Bundle "tomtom/tlib_vim"
Bundle "garbas/vim-snipmate"
Bundle "jimmyhchan/dustjs.vim"

Plugin 'maksimr/vim-jsbeautify'
Plugin 'tpope/vim-dispatch'
Plugin 'einars/js-beautify'
Plugin 'Chiel92/vim-autoformat'
Plugin 'ekalinin/Dockerfile.vim'
Bundle 'godlygeek/tabular'
Plugin 'Shougo/vimshell.vim'
Plugin 'Shougo/vimproc.vim'
Plugin 'keith/gist.vim'
Plugin 'ryanoasis/vim-devicons'
Plugin 'jacoborus/tender.vim'

" Optional:
"Bundle "honza/vim-snippets"
call vundle#end()            " required
filetype plugin indent on    " required
set encoding=utf-8
" Turn on line numbers
set number
" Relative line numbers
set rnu

" Make backspace delete characters
set backspace=2

" I like my leader being ','. It's easier to reach
" It defaults to ]
let mapleader = ","

" Disable backup and swap files
set nobackup
set noswapfile


" Improve Unix/Windows compatibility
set viewoptions=folds,options,cursor,unix
" Allow the cursor to pass the last character
set virtualedit=onemore
" Store more command history
set history=1000
" Store a bunch of undo history
set undolevels=4000
" Allow buffer switching without saving
set hidden

" Show matching brackets/parenthesis
set showmatch
" Don't blink
set matchtime=0
" Find as you type search
set incsearch
" Highlight search terms
set hlsearch
" Windows can be 0 line high
set winminheight=0
" Case insensitive search
set ignorecase
" Case sensitive if we type an uppercase
set smartcase

" tabs to spaces & set width to be 2
set expandtab
set shiftwidth=2
set softtabstop=2

" dust-related
let g:NERDCustomDelimiters = { 'dustjs': { 'left': '{!', 'right': '!}' } }
let g:surround_{char2nr('d')} = "{\r}"
" end dust-related

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" auto complete menu
set wildmenu

" highlight current line white space before cursor
set list
set listchars=trail:.
" set ember scope to load for all js files
let g:snipMate = {}
let g:snipMate.scope_aliases = {}
let g:snipMate.scope_aliases['javascript'] = 'javascript,ember'
let g:session_autoload = 'no'
let g:session_autosave_periodic = 1
let g:session_autosave = 'yes'
" Emad's tweaks
map <Leader> <Plug>(easymotion-prefix)
" indent file
nmap <silent> <leader>b gg=G
nmap <Tab> :bn<CR>
nmap <S-Tab> :bp<CR>
imap hh <Esc>
inoremap <c-o> <Esc>:w<CR>
noremap <c-o> <Esc>:w<CR>
"inoremap <c-s> <Esc>:w<CR>
"noremap <c-s> <Esc>:w<CR>

vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

"au BufWrite * :Autoformat
let g:formatters_rb = ['']

"Plain tasks

" close buffer without closing split
nmap <silent> <leader>d :bp\|bd #<CR>

" Move between windows
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Resize windows
map <S-Left> <C-w><
map <S-Down> <C-w>-
map <S-Up> <C-w>+
map <S-Right> <C-w>>

" use <leader>d to delete a line without adding it to the yanked stack
"nnoremap <silent> <leader>d "_d
"vnoremap <silent> <leader>d "_d

" use <leader>c to replace text without yanking replaced text
"nnoremap <silent> <leader>c "_c
"vnoremap <silent> <leader>c "_c

" yank/paste to/from the OS clipboard
noremap <silent> <leader>y "+y
noremap <silent> <leader>Y "+Y
noremap <silent> <leader>p "+p
noremap <silent> <leader>P "+P
set clipboard=unnamed
" paste without yanking replaced text in visual mode
"vnoremap <silent> p "_dP
"vnoremap <silent> P "_dp

"vmap <Tab> >
"vmap <S-Tab> <

" Delete buffer without destroying window
"map <leader>q :bp<bar>sp<bar>bn<bar>bd<CR>

"Enter add line in normal mode
nmap <CR> o<Esc>

" Deselect selected text
nnoremap <Leader>/ :nohlsearch<CR>

" opens $MYVIMRC for editing, or use :tabedit $MYVIMRC
nmap <Leader>v :tabedit $MYVIMRC<CR>

" Nerdtree
nmap <Leader>n :NERDTreeFind<CR>
nmap <Leader>m :NERDTreeToggle<CR>
"nmap <Leader>t :NERDTreeFocusToggle<CR>
nmap <Leader>t :NERDTreeFocus<CR>
"nmap <Leader>t :NERDTreeTabsToggle<CR>
"let g:nerdtree_tabs_autofind = 1
let NERDTreeIgnore=['node_modules','bower_components','tmp','dist','\~$']

" Statusbar
set laststatus=2

"let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
let g:OmniSharp_selector_ui = 'ctrlp'
if executable('ag')
  let g:ctrlp_user_command = 'ag %s -l -i --nocolor -g ""'
  " ag is fast enough that CtrlP doesn't need to cache
  "let g:ctrlp_use_caching = 0
endif

" Ignore some folders and files for CtrlP indexing
let g:ctrlp_custom_ignore = {
  \ 'dir':
  \ '\v\.git$|\.sass-cache$|\.hg$|\.svn$|\.yardoc|node_modules$|tmp/|bower_components|\.idea$|vendor|dist|cordova',
  \ 'file': '\.so$\|\.dat$|\.DS_Store$'
  \ }

let g:ctrlp_working_path_mode = 0
"let g:ctrlp_root_markers = ['.ctrlp']
"let g:NERDTreeChDirMode = 2

" UI
set t_Co=256
syntax on

set nospell
:map <F5> :setlocal spell! spelllang=en_us<CR>

" Theme
syntax enable
colorscheme tender
set background=dark
" set airline theme
let g:airline_theme = 'tender'
"colorscheme wombat256i
"colorscheme solarized

set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/

"GitGutter
highlight clear SignColumn

fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

autocmd BufNewFile,BufRead *.js.es6 set syntax=javascript
autocmd BufNewFile,BufRead *.hb set syntax=mustache
autocmd BufNewFile,BufRead *.hbs set syntax=mustache

" MatchTagAlways
let g:mta_filetypes = {
    \ 'html' : 1,
    \ 'mustache' : 1,
    \ 'xhtml' : 1,
    \ 'xml' : 1
    \}
let g:airline#extensions#tabline#enabled = 1
set timeoutlen=1000 ttimeoutlen=0

"let g:editorconfig_Beautifier = '~/src/configs/editorconfig'
"autocmd FileType javascript noremap <buffer>  <c-s-b> :call JsBeautify()<cr>
autocmd FileType javascript noremap <buffer>  <c-s-b> :Autoformat<cr>
autocmd FileType json noremap <buffer>  <c-s-b> :call JsonBeautify()<cr>

" for html
autocmd FileType html noremap <buffer> <c-s-b> :call HtmlBeautify()<cr>
autocmd FileType dust noremap <buffer> <c-s-b> :call HtmlBeautify()<cr>
autocmd FileType html.handlebars noremap <buffer> <c-s-b> :Autoformat handlebars<cr>
autocmd FileType hbs noremap <buffer> <c-s-b> :Autoformat handlebars<cr>
autocmd FileType mustache noremap <buffer> <c-s-b> :Autoformat handlebars<cr>
" for css or scss
autocmd FileType css noremap <buffer> <c-s-b> :call CSSBeautify()<cr>
autocmd FileType scss noremap <buffer> <c-s-b> :call CSSBeautify()<cr>
" for elixir
autocmd FileType elixir noremap <buffer> <c-s-b> :Autoformat<cr>

"ag -silver seracher
let g:ackprg = 'ag --nogroup --nocolor --column -i'

"enable unlimited undo
let undodir='$HOME/.vim/undo' " where to save undo histories

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    " Create dirs
    call system('mkdir ' . undodir)
    set undofile
    set undolevels=1000         " How many undos
    set undoreload=10000        " number of lines to save for undo
endif
